
const FIRST_NAME = "Iarina";
const LAST_NAME = "Cristea";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Caches{
    
    constructor(){
     this.about=0;
     this.home=0;
     this.contact=0;
    }
 
    
 }

Caches.prototype.pageAccessCounter= function pageAccessCounter(page){
    if(page==undefined || page.toLowerCase()=='home')
        this.home++;
    else if(page.toLowerCase()=='about')
            this.about++;
            else if(page.toLowerCase()=='contact')
            this.contact++;

 }
 

Caches.prototype.getCache=function getCache(){
    return this;
}


function initCaching() {
   var cache=new Caches();

    return cache;

}





module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

